let dice = "";
let scoreRound0 = "";
let scoreRound1 = "";
let scoreGlobal0 = "";
let scoreGlobal1 = "";
let n = 0;

function rollDice() {
    dice = Math.floor(Math.random() * 6) + 1;
    document.getElementById("dee").innerHTML = dice;

    if (dice == 1) {
        document.getElementById('dede').src = ("1.png");
    } else if (dice == 2) {
        document.getElementById('dede').src = ("2.png");
    } else if (dice == 3) {
        document.getElementById('dede').src = ("3.png");
    } else if (dice == 4) {
        document.getElementById('dede').src = ("4.png");
    } else if (dice == 5) {
        document.getElementById('dede').src = ("5.png");
    } else if (dice == 6) {
        document.getElementById('dede').src = ("6.png");
    }

    if (dice == 1 && n == 0) {
        scoreRound0 = 0;
        n = 1;
        document.getElementById("sr1").innerHTML = scoreRound0;
    } else if (dice > 1 && n == 0) {
        scoreRound0 = dice * 1 + scoreRound0 * 1;
        n = 0;
        document.getElementById("sr1").innerHTML = scoreRound0;
    } else if (dice == 1 && n == 1) {
        scoreRound1 = 0;
        n = 0;
        document.getElementById("sr2").innerHTML = scoreRound1;
    } else if (dice > 1 && n == 1) {
        scoreRound1 = scoreRound1 * 1 + dice * 1;
        n = 1;
        document.getElementById("sr2").innerHTML = scoreRound1;
    }
}

function hold() {

    if (scoreRound0 > 0) {
        scoreGlobal0 = scoreGlobal0 * 1 + scoreRound0 * 1;
        document.getElementById("sg1").innerHTML = scoreGlobal0;
        scoreRound0 = 0;
        document.getElementById("sr1").innerHTML = scoreRound0;
        n = 1;
        if (scoreGlobal0 >= 100) {
            alert("le joueur 1 a gagné!!!")
        }
    }
    if (scoreRound1 > 0) {
        scoreGlobal1 = scoreGlobal1 * 1 + scoreRound1 * 1;
        document.getElementById("sg2").innerHTML = scoreGlobal1;
        scoreRound1 = 0;
        document.getElementById("sr2").innerHTML = scoreRound1;
        n = 0;
        if (scoreGlobal1 >= 100) {
            alert("le joueur 2 a gagné!!!")
        }
    }
}

function newGame() {
    document.getElementById("dee").innerHTML = 0;
    scoreRound0 = 0;
    document.getElementById("sr1").innerHTML = scoreRound0;
    scoreRound1 = 0;
    document.getElementById("sr2").innerHTML = scoreRound1;
    scoreGlobal0 = 0;
    document.getElementById("sg1").innerHTML = scoreGlobal0;
    scoreGlobal1 = 0;
    document.getElementById("sg2").innerHTML = scoreGlobal1;

}